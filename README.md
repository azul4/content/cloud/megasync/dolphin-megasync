# dolphin-megasync

Extension for KDE based file managers to interact with Megasync

https://mega.nz/linux/repo/Arch_Extra/x86_64/

https://mega.nz/linux/MEGAsync/Arch_Extra/x86_64

<br>

How to clone this repository:
```
git clone https://gitlab.com/azul4/content/cloud/megasync/dolphin-megasync.git
```
